tput civis
while true; 
do
    clear

    COUNT=0
    while [[ COUNT -lt (($LINES/2)-4) ]];
    do
        echo ""
        let COUNT=$COUNT+1
    done
    echo ""
    date +"%H : %M : %S" | figlet -c -t -f slant | lolcat
    sleep 1
done
