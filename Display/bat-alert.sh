#!/bin/zsh

# *INSTALL******
#
# cp batalert.sh /usr/share/scripts/batalert.sh
#
# /etc/systemd/system/batalert.service
# 	[Unit]
# 	Description=Check battery state and alert with i3-nagbar
#
#	[Service]
#	Type=oneshot
#	ExecStart=/usr/share/scripts/batalert.sh
#
# /etc/systemd/system/batalert.timer
#	[Unit]
# 	Description= Periodically check battery state and alert with i3-nagbar
#
#	[Timer]
#	OnUnitActiveSec=60s
#	OnBootSec=30s
#
#	[Install]
#	WantedBy=timers.target
#
# systemctl daemon-reload
#
# systemctl enable batalert.timer
# systemctl start batalert.timer
#
# systemctl list-timers --all
# systemctl status batalert.timer
# systemctl status batalert.service
#
# *CONFIG*******
#
WARNING_PERCENTAGE=20
#
# *CODE*********

#i3-nagbar -t warning -m "Test"
export DISPLAY=:0.0

if [ $(cat /sys/class/power_supply/BAT0/capacity) -lt $WARNING_PERCENTAGE ] && \
[ $(cat /sys/class/power_supply/BAT0/status) = "Discharging" ] && \
[[ ! -f ".bat-alert.lock" ]];
then
    i3-nagbar -t warning -m "Low Battery ($(cat /sys/class/power_supply/BAT0/capacity)%)" -B "Shut down" "systemctl poweroff" -B "Hibernate" "systemctl hibernate" 
    touch ".bat-alert.lock"
else
    if [[ -f ".bat-alert.lock" ]];
    then
        rm ".bat-alert.lock"
    fi
fi
