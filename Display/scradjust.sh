#!/bin/sh
# extend non-HiDPI external display on DP* above HiDPI internal display eDP*
# see also https://wiki.archlinux.org/index.php/HiDPI
# you may run into https://bugs.freedesktop.org/show_bug.cgi?id=39949
#                  https://bugs.launchpad.net/ubuntu/+source/xorg-server/+bug/883319

echo
echo " Set screen Layout"
echo " ---------------------------------------"

echo
echo " 0: Reset"

echo
echo " 1: Place EXT on bottom left of INT"
echo "            [     ]"
echo "    [     ] [ INT ]"
echo "    [ EXT ] [     ]"

echo
echo " 2: Place EXT on top left of INT"
echo "    [ EXT ] [     ]"
echo "    [     ] [ INT ]"
echo "            [     ]"

echo
echo " 3: Place EXT on top right of INT"
echo "    [     ] [ EXT ]"
echo "    [ INT ] [     ]"
echo "    [     ]"

echo
echo " 4: Place EXT on bottom right of INT"
echo "    [     ]"
echo "    [ INT ] [     ]"
echo "    [     ] [ EXT ]"

echo
echo " 5: Place EXT on top of INT"
echo "    [ EXT ]"
echo "    [_____]"
echo "    [     ]"
echo "    [ INT ]"

echo
echo " ---------------------------------------"

vared -p ' Choose screen Layout: ' -c selection
vared -p ' Set screen Rotation (normal, left, right, inverted): ' -c rotation


if [ "$rotation" = "" ]; then
	rotation="normal"
fi

echo
echo " Applying Layout..."

# Set Dimensions
int_scale=1x1
ext_scale=1.25x1.25

EXT=`xrandr --current | sed 's/^\(.*\) connected.*$/\1/p;d' | grep -v ^eDP | head -n 1`
INT=`xrandr --current | sed 's/^\(.*\) connected.*$/\1/p;d' | grep -v ^DP | head -n 1`

ext_w=`xrandr | sed 's/^'"${EXT}"' [^0-9]* \([0-9]\+\)x.*$/\1/p;d'`
ext_h=`xrandr | sed 's/^'"${EXT}"' [^0-9]* [0-9]\+x\([0-9]\+\).*$/\1/p;d'`
int_w=`xrandr | sed 's/^'"${INT}"' [^0-9]* \([0-9]\+\)x.*$/\1/p;d'`
int_h=`xrandr | sed 's/^'"${INT}"' [^0-9]* [0-9]\+x\([0-9]\+\).*$/\1/p;d'`

off_w=`echo $(( ($int_w-$ext_w)/2 )) | sed 's/^-//'`
diff_h=`echo $(( ($int_h-$ext_h) )) | sed 's/^-//'`

# Disable screen
xset dpms force off

# Reset and re-init xrandr to auto settings
sleep 0.5
xrandr -s 0
xrandr --auto
sleep 1

# Apply Layout
if [ "$selection" = "1" ]; then
    xrandr --output "${INT}" --primary --auto --pos ${ext_w}x0 --scale ${int_scale}         --output "${EXT}" --auto --scale ${ext_scale} --pos 0x0 --rotate ${rotation}
    elif [ "$selection" = "2" ]; then
    xrandr --output "${INT}" --primary --auto --pos ${ext_w}x${diff_h} --scale ${int_scale} --output "${EXT}" --auto --scale ${ext_scale} --pos 0x0 --rotate ${rotation}
    elif [ "$selection" = "3" ]; then
    xrandr --output "${INT}" --primary --auto --pos 0x${diff_h} --scale ${int_scale}        --output "${EXT}" --auto --scale ${ext_scale} --pos ${int_w}x0 --rotate ${rotation}
    elif [ "$selection" = "4" ]; then
    xrandr --output "${INT}" --primary --auto --pos 0x0 --scale ${int_scale}                --output "${EXT}" --auto --scale ${ext_scale} --pos ${int_w}x0 --rotate ${rotation}
    elif [ "$selection" = "5" ]; then
    xrandr --output "${INT}" --primary --auto --pos 0x${ext_h} --scale ${int_scale}         --output "${EXT}" --auto --scale ${ext_scale} --pos 0x0 --rotate ${rotation}
fi

# Reenable screen
xset dpms force on

echo " Layout applied :D"
echo
