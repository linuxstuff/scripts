#!/bin/sh
xrandr --output DP-0 --primary --mode 1920x1080 --rate 144 --pos 0x0 --rotate normal --output DP-1 --off --output DP-2 --off --output DP-3 --mode 1600x1200 --rate 120 --pos 1920x0 --rotate left --output HDMI-0 --off --output DP-4 --off --output DP-5 --off
