#!/bin/bash

# Based on https://gist.github.com/sebastiencs/5d7227f388d93374cebdf72e783fbd6a
# Designed for dunst
# 
# You can call this script like this:
# $./volume.sh up
# $./volume.sh down
# $./volume.sh mute

PREFIX="Volume: "

APP_ID="2593"
TIMEOUT="5000"

CHAR_WHOLE="■"
CHAR_HALF="▬"
CHAR_END="□"
CHAR_MUTE="◇"

# Send a dunst notification
function notify() {
    dunstify "$PREFIX$1" -u normal -r "$APP_ID" -t "$TIMEOUT"
}

function get_volume {
    amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
}

function is_mute {
    amixer get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
}

function send_notification {
    volume=$(get_volume)
    
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
    bar=$(seq -s "$CHAR_WHOLE " $((volume / 10)) | sed 's/[0-9]//g')

    if [ $((volume % 10)) -eq 0 ]; then
        bar+="$CHAR_WHOLE "
    else
        if [ "$volume" -gt 10 ] ; then
            bar+="$CHAR_WHOLE "
        fi
        bar+="$CHAR_HALF "
    fi


    if [ "$volume" -eq 100 ] || [ "$volume" -eq 0 ]; then
        bar="${bar%??}$CHAR_END"
    fi

    while [ ${#bar} -lt 20 ] ; do 
        bar+=' '
    done

    # Send the notification
    notify "[ $bar] $(printf " (%3d%%)" "$volume")"
}

case $1 in
    up)
        # Set the volume on (if it was muted)
        amixer -D pulse set Master on > /dev/null
        # Up the volume (+ 5%)
        amixer -D pulse sset Master 5%+ > /dev/null
        send_notification
	;;
    down)
        amixer -D pulse set Master on > /dev/null
        amixer -D pulse sset Master 5%- > /dev/null
        send_notification
	;;
    mute)
    	# Toggle mute
	    amixer -D pulse set Master 1+ toggle > /dev/null
        if is_mute ; then
            # Send the notification
            notify "$CHAR_MUTE"
        else
            send_notification
        fi
	;;
esac