#! /bin/sh

killall -q polybar
while pgrep -x polybar >/dev/null; do sleep 1; done

i3 reload
sh ~/.config/polybar/launch_polybar.sh

