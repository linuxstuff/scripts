#!/bin/sh

yay -Syy --sudoloop --noconfirm \
\
git \
man \
ufw \
tldr \
micro \
htop \
neofetch \
numlockx \
xdotool \
xclip \
tree \
screen \
trash-cli \