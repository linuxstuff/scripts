#!/bin/sh

yay -Syy --sudoloop --noconfirm \
\
playerctl \
pulseaudio \
pulseaudio-alsa \
pulseaudio-bluetooth \
cli-visualizer \