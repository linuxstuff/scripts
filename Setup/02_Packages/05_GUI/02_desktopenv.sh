#!/bin/sh

yay -Syy --sudoloop --noconfirm \
\
i3-gaps \
i3lock-color \
polybar \
rofi \
polkit-gnome \
notify-osd \
flameshot \
scrot \
unclutter \
network-manager-applet \