#!/bin/sh

yay -Syy --sudoloop --noconfirm \
\
fontconfig \
lxappearance \
kvantum-qt5 \
lightdm-gtk-greeter-settings \
nitrogen \
ttf-font-awesome \
ttf-font-awesome-4 \
ttf-roboto \
ttf-windows \
papirus-icon-theme \
paper-gtk-theme-git \
adapta-gtk-theme \
adapta-gtk-theme-colorpack \
arc-gtk-theme \
ant-gtk-theme \
flat-remix \
breeze-default-cursor-theme

#nerd-fonts-complete \

