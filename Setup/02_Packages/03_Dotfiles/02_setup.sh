#!/bin/sh
git clone --recursive -j8 git@gitlab.com:personalisation/dotfiles.git
cp -r dotfiles/* ~
cp -r dotfiles/.* ~

mkdir -p ~/Userdata
cd Userdata
git clone --recursive -j8 git@gitlab.com:personalisation/scripts.git
mv scripts Scripts