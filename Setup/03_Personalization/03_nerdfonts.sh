#!/bin/sh

yay -Sy --sudoloop  \
nerd-fonts-anonymous-pro  \
nerd-fonts-arimo \
nerd-fonts-bitstream-vera-mono \
nerd-fonts-cascadia-code \
nerd-fonts-dejavu-complete \
nerd-fonts-droid-sans-mono \
nerd-fonts-fantasque-sans-mono \
nerd-fonts-fira-code \
nerd-fonts-gabmus \
nerd-fonts-go-mono \
nerd-fonts-hack \
nerd-fonts-hermit \
nerd-fonts-ibm-plex-mono \
nerd-fonts-inconsolata \
nerd-fonts-inconsolata-go \
nerd-fonts-inter \
nerd-fonts-iosevka \
nerd-fonts-jetbrains-mono \
nerd-fonts-liberation-mono \
nerd-fonts-meslo \
nerd-fonts-monoid \
nerd-fonts-mononoki \
nerd-fonts-mplus \
nerd-fonts-noto \
nerd-fonts-overpass \
nerd-fonts-profont \
nerd-fonts-ricty \
nerd-fonts-roboto-mono \
nerd-fonts-sf-mono \
nerd-fonts-source-code-pro \
nerd-fonts-terminus \
nerd-fonts-tinos \
nerd-fonts-ubuntu-mono \
nerd-fonts-victor-mono
