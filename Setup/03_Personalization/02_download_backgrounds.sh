#!/bin/sh

echo "Downloading Background zip"

mkdir -p ~/Pictures/

wget https://cloud.sgruber.at/index.php/s/zSkg4pof5Z8s429/download -O backgrounds.zip
unzip backgrounds.zip
mv Backgrounds ~/Pictures/Backgrounds
rm backgrounds.zip

echo "Backgrounds downloaded and placed! Use nitrogen or something"