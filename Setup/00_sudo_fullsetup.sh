#!/bin/sh

sh "01_Base/01_userdirs.sh"

sh "02_Packages/01_aurhelper.sh"
sh "02_Packages/02_systemupgrade.sh"

sh "02_Packages/03_Dotfiles/01_yadm.sh"
sh "02_Packages/03_Dotfiles/02_setup.sh"

sh "02_Packages/04_CLI/01_base.sh"
sh "02_Packages/04_CLI/02_tools.sh"
sh "02_Packages/04_CLI/03_connectivity.sh"
sh "02_Packages/04_CLI/04_audio.sh"
sh "02_Packages/04_CLI/05_sensors.sh"

sh "02_Packages/05_GUI/01_displayserver.sh"
sh "02_Packages/05_GUI/02_desktopenv.sh"
sh "02_Packages/05_GUI/03_tools.sh"
sh "02_Packages/05_GUI/04_themes.sh"
sh "02_Packages/05_GUI/05_coding.sh"
sh "02_Packages/05_GUI/06_entertainment.sh"
sh "02_Packages/05_GUI/07_gaming.sh"

sh "03_Personalization/01_pacman_conf.sh"
sh "03_Personalization/02_download_backgrounds.sh"
