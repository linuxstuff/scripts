#!/bin/sh

echo "Creating userdirs"

mv ~/Desktop ~/Desktop_old
mv ~/Documents ~/Documents_old
mv ~/Downloads ~/Downloads_old
mv ~/Pictures ~/Pictures_old
mv ~/Scripts ~/Scripts_old
mv ~/Videos ~/Videos_old

echo "Creating dirs"

mkdir -p ~/Userdata
mkdir -p ~/Userdata/Desktop
mkdir -p ~/Userdata/Documents
mkdir -p ~/Userdata/Downloads
mkdir -p ~/Userdata/Pictures
mkdir -p ~/Userdata/Scripts
mkdir -p ~/Userdata/Videos

echo "Creating symlinks"

ln -s /home/${USER}/Userdata/Desktop /home/${USER}/Desktop
ln -s /home/${USER}/Userdata/Documents /home/${USER}/Documents
ln -s /home/${USER}/Userdata/Downloads /home/${USER}/Downloads
ln -s /home/${USER}/Userdata/Pictures /home/${USER}/Pictures
ln -s /home/${USER}/Userdata/Scripts /home/${USER}/Scripts
ln -s /home/${USER}/Userdata/Videos /home/${USER}/Videos
